# Maxim Dumortier started on 2022-09-22
# Backup only the data from Home Assistant (HA) with InfluxDB
# Each measurement will be backup in a CSV file for each month
# Use Pandas to export data in Excel format
# ! Pay attention if you try it with a big timespan it may take a really long time to run depending on the size of the DB

import pandas as pd
import numpy as np
from influxdb import DataFrameClient
from datetime import datetime, date
from dateutil.relativedelta import relativedelta

# Parameters of the script:
IP_addr = '127.0.0.1' # IP address of the HA server (or DNS name)
usr = 'my username' # Username to connect to the InfluxDB instance
pwd = '12345678' # Password to connect to the InfluxDB instance

StartDate= date(2019,3,1) # Year, Month, Day
EndDate = date(2022,8,1) # End date included
delta = relativedelta(months = 1) # Do the query for each month, also save the data in a monthly CSV

Meas = "W" # Measurement to backup, select it in the list printed with client.query('SHOW MEASUREMENTS')
# TODO : Automate the backup of different measurements at once (list measurements to backup)

# Start the connection:
client = DataFrameClient(host=IP_addr, port=8086, username=usr, password=pwd) # Port 8086 is the default port for InfluxDB
client.switch_database('homeassistant') # Select the right DB, to figure out : client.get_list_database()
print(client.query('SHOW MEASUREMENTS')) # Display the available measurements for future backups of other measurements


while StartDate <= EndDate: # Loop on each month (or other if delta is changed)
    print(StartDate.strftime("%Y-%m-%d")) # lets show the advance of our backup process

    StartDateMs = datetime.strptime(StartDate.strftime("%Y-%m-%d") + ' 00:00:00','%Y-%m-%d %H:%M:%S').timestamp()*1000 # Convert the date into ms (used by InfluxDB)
    EndDateMs = datetime.strptime((StartDate+ delta).strftime("%Y-%m-%d")+' 00:00:00','%Y-%m-%d %H:%M:%S').timestamp()*1000
    try: # Need to try because if the measurement is not there for the concerned month, we get a "KeyError" from the DB
        query = 'SELECT * FROM "autogen"."' + Meas + '" WHERE time >= ' + str(int(StartDateMs))+ 'ms and time <= ' + str(int(EndDateMs)) +'ms'
        #print(query)
        results = client.query(query)

        print("Request Done")
        middle_dict={} # Create dict to order data from query into pandas compatible dict
        for l in results[Meas]:
            # Delete the useless columns, we just keep the time (Index), entity_id, friendly_name_str, and value(will be renamed later)
            if(l == "entity_id" or l == "friendly_name_str" or l =="value"):
                middle_dict[l] = results[Meas][l]

        df = pd.DataFrame(middle_dict) # Create the dataframe starting with the dict created {col1:[data1], col2:[data2],...}
        # Parse date into CET (Brussels time) timezone and format it into excel possible format
        df.index = pd.to_datetime(df.index.tz_convert('CET'),format="%Y-%m-%d %H:%M:%S.%f%z").strftime('%Y-%m-%d %H:%M:%S.%f')
        # Change the "value" column into the current unit
        df.rename(columns ={"value":Meas},inplace = True)

        grouped = df.groupby(df['entity_id'])
        print(df['entity_id'].unique())
        for name in df['entity_id'].unique():
            temp_df = grouped.get_group(name)
            friendly_name = temp_df["friendly_name_str"].unique()[0]
            # Delete columns entity_id and freindly_name_str (all the lines contain the same values in these columns ... useless. They're put into the title
            temp_df.pop("friendly_name_str")
            temp_df.pop('entity_id')
            temp_df.to_excel(str(name)  + "_" + str(friendly_name) + StartDate.strftime("_%Y-%m")+ ".xlsx")
    except KeyError:
        print("No '" + Meas + "' For " + StartDate.strftime("%Y-%m-%d"))
            
    # Go to the next month
    StartDate = StartDate + delta



# Python Home Assitant InfluxDB Backup


## Getting started
This is a simple script to export the data from your Home Assitant (HA) server. It works with the InfluxDB of your HA. It will save your data into Excel files (ont for each month of each data). 


## Name
Python Home Assistant (HA) InfluxDB Backup

## Description
The python program will scan the InfluxDB and export your data into Excel spreadsheets. It may take some time depdening on the amount of data you have.



## Installation
Just download or copy the content.
You'll have to download to folloing lib:

pip install pandas  (https://pypi.org/project/pandas/)

pip install numpy (https://pypi.org/project/numpy/)

pip install influxdb (https://pypi.org/project/influxdb/)

Then run it with your parameters. 
Enjoy.

## Usage
Full comments in the code, it aint much.

## Support
Well, good luck !

## Roadmap
Possible to make the code more adaptative to other HA servers. 

Possible to make a 'auto' save of all the measurements at once (just list the measurements and loop on them). This 
may just result in a very huge run time of the python code ... to be tested

Why not add the possibility to use other DB than InfluxDB

## Authors and acknowledgment
Initial code by Maxim Dumortier sept 2022

## License
WTFPL

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
  
http://www.wtfpl.net/

## Project status
The projet is thrown as is. Currently it was already used several times to make the backup of the data of our Home Assistant. It's very simple and sufficient to us.
